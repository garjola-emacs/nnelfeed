;;; nnelfeed.el --- Elfeed backend for Gnus -*- lexical-binding: t -*-

;; Copyright (C) 2021 Garjola Dindi

;; Author: Garjola Dindi <garjola@garjola.net>
;; URL: https://garjola.net
;; Version: 0.1.0
;; Package-Requires: ((emacs "27.1"))

;; This file is NOT part of GNU Emacs.

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:
;;
;; A Gnus backend to read RSS and Atom feeds using elfeed behind the scenes
;; https://gitlab.com/garjola-emacs/nnelfeed.git

;;; Code:

(require 'gnus)
(require 'nnoo)
(require 'nnmail)
(require 'message)
(require 'mm-util)
(require 'gnus-util)


(nnoo-declare nnelfeed)


;;; Interface functions.
(nnoo-define-basics nnelfeed)

(deffoo nnelfeed-retrieve-headers (articles &optional group server _fetch-old))
(deffoo nnelfeed-open-server (server &optional defs _connectionless))
(deffoo nnelfeed-request-group (group &optional server dont-check _info))
(deffoo nnelfeed-close-group (_group &optional _server))
(deffoo nnelfeed-request-article (article &optional group server buffer))
(deffoo nnelfeed-request-expire-articles)
(deffoo nnelfeed-request-delete-group (group &optional _force server))
(deffoo nnelfeed-request-list-newsgroups (&optional server))
(deffoo nnelfeed-retrieve-groups (groups &optional server))
(nnoo-define-skeleton nnelfeed)

(unless (assoc "nnelfeed" gnus-valid-select-methods)
  (gnus-declare-backend "nnelfeed" 'none 'respool 'address))

(provide 'nnelfeed)
