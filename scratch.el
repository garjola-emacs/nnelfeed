;;; nnelfeed.el --- Elfeed backend for Gnus -*- lexical-binding: t -*-

;; Copyright (C) 2021 Garjola Dindi

;; Author: Garjola Dindi <garjola@garjola.net>
;; URL: https://garjola.net
;; Version: 0.1.0
;; Package-Requires: ((emacs "27.1"))

;; This file is NOT part of GNU Emacs.

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:
;;
;; A Gnus backend to read RSS and Atom feeds using elfeed behind the scenes
;; https://gitlab.com/garjola-emacs/nnelfeed.git

;;; Code:


(require 'elfeed)
(setq elfeed-feeds 
      '(("https://www.emacswiki.org/emacs?action=rss" emacswiki)
        ("https://www.mediapart.fr/articles/feed" mediapart)))

(elfeed-feed-list)
(elfeed-update)
(elfeed-update-feed)
(elfeed-feed-entries "https://www.emacswiki.org/emacs?action=rss")
(car (elfeed-feed-entries "https://www.mediapart.fr/articles/feed"))


(with-current-buffer (get-buffer-create "Elfeed Article Gnus")
  (let* ((feed "https://www.mediapart.fr/articles/feed")
         (entry (car (elfeed-feed-entries feed))))
    (gnus-article-mode)
      (setq elfeed-show-entry entry)
      (elfeed-show-refresh)))
